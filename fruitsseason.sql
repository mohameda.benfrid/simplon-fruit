-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 04 nov. 2022 à 14:00
-- Version du serveur : 10.4.25-MariaDB
-- Version de PHP : 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `fruitsseason`
--

-- --------------------------------------------------------

--
-- Structure de la table `color`
--

CREATE TABLE `color` (
  `color_id` int(8) NOT NULL,
  `color_name` varchar(128) NOT NULL,
  `color_hexa` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `color`
--

INSERT INTO `color` (`color_id`, `color_name`, `color_hexa`) VALUES
(1, 'White', '#fdfefe '),
(2, 'Black', '#111111'),
(3, 'Red', '#e74c3c '),
(4, 'Green', '#27ae60 '),
(5, 'Blue', '#34495e '),
(6, 'Yellow', '#f7dc6f '),
(7, 'Orange', '#dc7633 '),
(8, 'Pink', '#c39bd3 '),
(9, 'Brown', '#6e2c00 '),
(10, 'several colors', '/////');

-- --------------------------------------------------------

--
-- Structure de la table `fruit`
--

CREATE TABLE `fruit` (
  `fruit_id` int(8) NOT NULL,
  `fruit_exotic` tinyint(1) NOT NULL,
  `fruit_name` varchar(128) NOT NULL,
  `fruit_color` int(8) NOT NULL,
  `fruit_description` mediumtext DEFAULT NULL,
  `fruit_img` varchar(255) DEFAULT NULL,
  `fruit_nutrition` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `fruit`
--

INSERT INTO `fruit` (`fruit_id`, `fruit_exotic`, `fruit_name`, `fruit_color`, `fruit_description`, `fruit_img`, `fruit_nutrition`) VALUES
(1, 0, 'Cherry', 3, 'a small, round, soft red or black fruit with a single hard seed in the middle, or the tree on which the fruit grows', 'https://assets.bonappetit.com/photos/60ef61f34d6cfb72ea6baeec/master/pass/Sweet-Sour-Cherries.jpg', ''),
(2, 1, 'Banana', 6, 'a long, curved fruit with a yellow skin and soft, sweet, white flesh inside:\r\na bunch of bananas\r\nbanana milkshake', 'https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/6/5/e/65e454e5f9_50180028_banane.jpg', ''),
(3, 0, 'Apple', 10, 'a round fruit with firm, white flesh and a green, red, or yellow skin: \r\nto peel an apple\r\napple pie/sauce\r\nan apple tree', 'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/apples-700x350-edfec3b.png', ''),
(4, 0, 'Apricot', 7, 'a small, round, soft fruit with a pale orange, furry skin', 'https://www.papillesetpupilles.fr/wp-content/uploads/2013/01/Abricots-%C2%A9-Pearl-diver-shutterstock.jpg', ''),
(5, 0, 'Pear', 6, 'a sweet fruit, usually with a green skin and a lot of juice, that has a round base and is slightly pointed towards the stem', 'https://www.tastingtable.com/img/gallery/why-you-should-leave-the-skin-on-pears/l-intro-1653326060.jpg', ''),
(6, 0, 'Peach', 7, 'a round fruit with sweet yellow flesh that has a lot of juice, a slightly furry red and yellow skin, and a large seed in its centre: \r\nWould you like peaches and cream for dessert?', 'https://www.wickhosp.com/wp-content/uploads/2021/08/buy-peaches-online-073119.jpg', ''),
(7, 0, 'Plum', 5, 'a small, round fruit with a thin, smooth, red, purple, or yellow skin, sweet, soft flesh, and a single large, hard seed; the tree on which this fruit grows:\r\nplum jam\r\na plum tree\r\nGrandad planted a lot of fruit trees, including gooseberry, blackcurrant, plum, and loganberry.\r\n', 'https://healthyfamilyproject.com/wp-content/uploads/2020/05/Plums-background.jpg', ''),
(8, 1, 'Watermelon', 4, 'a large, round or oval-shaped fruit with dark green skin, sweet pink flesh, and a lot of black seeds', 'https://www.tastingtable.com/img/gallery/16-ways-you-need-to-try-eating-watermelon/intro-1660763356.jpg', ''),
(9, 1, 'Melon', 10, 'a large, round fruit with hard yellow or green skin, sweet flesh, and a lot of seeds', 'https://img.passeportsante.net/1200x675/2021-05-03/i102090-melon-nu.jpg', ''),
(10, 0, 'Quince', 6, 'a hard fruit that looks like an apple and has a strong sweet smell:\r\nquince jam', 'https://www.pritikin.com/wp/wp-content/uploads/2016/09/quince-fruit-fall-food-600x296.jpg', ''),
(11, 0, 'Orange', 7, 'a round sweet fruit that has a thick orange skin and an orange centre divided into many parts:\r\na glass of orange juice', 'https://monjardinmamaison.maison-travaux.fr/wp-content/uploads/sites/8/2021/01/orange-9.jpg', ''),
(12, 0, 'Lemon', 6, 'a fruit similar to a lemon but larger, with a less sour taste and thick skin, or the small tree on which this fruit grows:\r\nI love citron, especially in marmalade.\r\ncitron flavoured noodles', 'https://cdn.britannica.com/84/188484-050-F27B0049/lemons-tree.jpg', ''),
(13, 0, 'Berry', 10, 'a small, round fruit on particular plants and trees', 'https://media.istockphoto.com/photos/wild-berry-mix-strawberries-blueberries-blackberries-and-raspberries-picture-id499658564?k=20&m=499658564&s=612x612&w=0&h=jWWLTXolflgwzPSQ-Hy7OkXFXZqSjJaSSL5BG0c34Ck=', ''),
(14, 0, 'Tomato', 3, 'a round, red fruit with a lot of seeds, eaten cooked or uncooked as a vegetable, for example in salads or sauces: \r\na sliced tomato\r\ntomato salad/soup\r\npasta with a tomato sauce', 'https://agriculture.gouv.fr/sites/default/files/15136_044_bd.jpg', ''),
(15, 0, 'Pomegranate', 3, 'a round, thick-skinned fruit containing a mass of red seeds and a lot of juice', 'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/health-benefits-of-pomegranate-main-image-700-350-5fa4b34.jpg', ''),
(16, 0, 'Fig', 5, 'a sweet, soft, purple, or green fruit with many seeds, or a tree on which these grow', 'https://www.lanutrition.fr/sites/default/files/styles/article_large/public/ressources/figues_3_6.jpg?itok=Xfn5A2Os', ''),
(17, 1, 'Coconut', 9, 'a large fruit like a nut with a thick, hard, brown shell covered in fibre (= a mass of thread-like parts) and containing hard, white flesh that can be eaten and a clear liquid:\r\na coconut shell\r\n', 'https://cdn.vanguardngr.com/wp-content/uploads/2019/10/coconut-shells.jpg', ''),
(18, 1, 'Lychee', 8, 'a fruit with a rough, brown shell and sweet, white flesh around a large, shiny, brown seed, or the evergreen tree (= one that never loses its leaves) on which this fruit grows', 'https://cdn.britannica.com/27/218927-050-E99E1D46/Lychee-fruit-tree-plant.jpg', ''),
(19, 0, 'Grape', 10, 'a small, round, purple or pale green fruit that you can eat or make into wine:\r\nblack/white/red/green grapes\r\na bunch of grapes\r\nseedless grapes\r\ngrape juice', 'https://i.enfant.com/1400x787/smart/2021/08/28/raisin.jpeg', ''),
(20, 0, 'Chestnut', 9, 'A man in the street was selling bags of roast chestnuts.', 'https://upload.wikimedia.org/wikipedia/commons/f/f1/Romagne_86_Ch%C3%A2taignes_2008.jpg', '');

-- --------------------------------------------------------

--
-- Structure de la table `link_fruit_month`
--

CREATE TABLE `link_fruit_month` (
  `link_fruit` int(8) NOT NULL,
  `link_month` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `link_fruit_month`
--

INSERT INTO `link_fruit_month` (`link_fruit`, `link_month`) VALUES
(1, 5),
(1, 7),
(3, 1),
(3, 2),
(3, 3),
(3, 9),
(3, 10),
(3, 11),
(3, 12),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 9),
(5, 10),
(5, 11),
(5, 12),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(7, 7),
(7, 8),
(7, 9),
(7, 10),
(10, 10),
(10, 11),
(10, 12),
(11, 1),
(11, 2),
(11, 3),
(11, 12),
(12, 1),
(12, 2),
(12, 3),
(12, 4),
(12, 5),
(12, 6),
(12, 7),
(12, 8),
(12, 9),
(14, 6),
(14, 7),
(14, 8),
(14, 9),
(14, 10),
(16, 7),
(16, 8),
(16, 9),
(16, 10),
(19, 8),
(19, 9),
(19, 10),
(20, 10),
(20, 11),
(20, 12);

-- --------------------------------------------------------

--
-- Structure de la table `link_fruit_recipe`
--

CREATE TABLE `link_fruit_recipe` (
  `link_fruit` int(8) NOT NULL,
  `link_recipe` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `month`
--

CREATE TABLE `month` (
  `month_order` int(8) NOT NULL,
  `month_name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `month`
--

INSERT INTO `month` (`month_order`, `month_name`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Structure de la table `recipe`
--

CREATE TABLE `recipe` (
  `recipe_id` int(8) NOT NULL,
  `recipe_name` varchar(128) NOT NULL,
  `recipe_description` longtext NOT NULL,
  `recipe_sweet` tinyint(1) NOT NULL,
  `recipe_ingredients` text NOT NULL,
  `recipe_img` varchar(255) NOT NULL,
  `recipe_fruit` int(8) NOT NULL,
  `recipe_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `recipe`
--

INSERT INTO `recipe` (`recipe_id`, `recipe_name`, `recipe_description`, `recipe_sweet`, `recipe_ingredients`, `recipe_img`, `recipe_fruit`, `recipe_time`) VALUES
(1, 'Tarte au pomme', '    Étape 1\r\n    pomme\r\n\r\n    Éplucher et découper en morceaux 4 Golden.\r\n    Étape 2\r\n    pomme\r\n    sucre vanillé\r\n\r\n    Faire une compote : les mettre dans une casserole avec un peu d\'eau (1 verre ou 2). Bien remuer. Quand les pommes commencent à ramollir, ajouter un sachet ou un sachet et demi de sucre vanillé. Ajouter un peu d\'eau si nécessaire.\r\n    Étape 3\r\n\r\n    Vous saurez si la compote est prête une fois que les pommes ne seront plus dures du tout. Ce n\'est pas grave s\'il reste quelques morceaux.\r\n    Étape 4\r\n    pomme\r\n\r\n    Pendant que la compote cuit, éplucher et couper en quatre les deux dernières pommes, puis, couper les quartiers en fines lamelles (elles serviront à être posées sur la compote).\r\n    Étape 5\r\n\r\n    Préchauffer le four à 210°C (thermostat 7).\r\n    Étape 6\r\n    pâte brisée\r\n\r\n    Laisser un peu refroidir la compote et étaler la pâte brisée dans un moule et la piquer avec une fourchette.\r\n    Étape 7\r\n    pomme\r\n    beurre\r\n\r\n    Verser la compote sur la pâte et placer les lamelles de pommes en formant une spirale ou plusieurs cercles, au choix ! Disposer des lamelles de beurre dessus.\r\n    Étape 8\r\n    sucre vanillé\r\n\r\n    Mettre au four et laisser cuire pendant 30 min max. Surveiller la cuisson. Vous pouvez ajouter un peu de sucre vanillé sur la tarte pendant que çà cuit pour caraméliser un peu.', 1, '', 'https://files.meilleurduchef.com/mdc/photo/recette/tarte-pommes-cap/tarte-pommes-cap-960.jpg', 3, '00:55:00'),
(2, 'Bananes au chocolat au lait', '\r\n1\r\n\r\nCassez le chocolat en morceaux et déposez-le dans un bol allant au micro-ondes.\r\n2\r\n\r\nAjoutez le lait et faites fondre le tout au micro-ondes pendant 2 minutes.\r\n3\r\n\r\nMélangez bien en fin de cuisson jusqu\'à temps que le chocolat soit bien fondu. Réservez au chaud.\r\n\r\n4\r\n\r\nEpluchez et coupez les bananes en rondelles. \r\n\r\n5\r\n\r\nFaites fondre le beurre doux dans une poêle sur feu doux.\r\n6\r\n\r\nDéposez-y les rondelles de bananes et saupoudrez-les directement de sucre vanillé. Faites-les ensuite caraméliser quelques minutes, en mélangeant régulièrement, jusqu’à ce qu’elles soient bien dorées.\r\n7\r\n\r\nQuand les rondelles de bananes sont bien caramélisées, déposez-les dans les assiettes et nappez-les de sauce au chocolat au lait.\r\n8\r\n\r\nServez de suite accompagné d’une boule de glace à la vanille. ', 1, '', 'https://assets.afcdn.com/recipe/20160316/63270_w1024h768c1cx2592cy1728.jpg', 2, '00:15:00'),
(3, 'Tajine d\'agneau aux abricots et aux amendes (Tadjin bel meshmesh)', 'Chauffer l\\\'huile dans une grande marmite. Ajouter l\\\'agneau et les faire cuire sur un moyen-vif pendant 3-4 minutes, jusqu\\\'à ce qu?il soit doré, en remuant souvent. Retirez l\\\'agneau et réservez.\r\n\r\nIncorporer l\\\'oignon et l\\\'ail dans la marmite et faire cuire doucement pendant 5 minutes, jusqu\\\'à ce qu?il soit bien ramolli. Remettre l\\\'agneau dans la marmite. Ajouter le bouillon, le zeste et jus d?orange, la cannelle, le miel et le sel et le poivre. Porter à ébullition puis réduire le feu, couvrir et laisser cuire doucement pendant 1 heure.\r\n\r\nAjouter les abricots et les deux-tiers de la menthe et faites cuire pendant 30 minutes, jusqu\\\'à ce que l\\\'agneau soit tendre. Incorporer les amandes puis laisser épaissir la sauce. Servir avec le reste de menthe et les \\\'amandes grillées éparpillées sur le tajine.', 0, '500g d\'épaule d?agneau en cubes     1 oignon, haché     2 gousses d\'ail, hachées     700ml de bouillon d\'agneau     2 c. à soupe d\'huile d\'olive     le jus de 1 orange + le zeste râpé     200g d?abricots secs     1 c. à thé de miel d?acacia     1 bâton de cannelle     3 c. à soupe de menthe fraîche hachée     25g d\'amandes grillées     De la semoule de couscous pour servir     25g d\'amandes entières', 'https://www.la-cuisine-marocaine.com/photos-recettes/01-tajine-d-agneau-aux-abricots-et-amandes.jpg', 4, '02:00:00'),
(4, 'Clafoutis aux cerises', '    Étape 1\r\n    cerises\r\n    beurre\r\n    farine\r\n    sucre en poudre\r\n    sel\r\n    sucre vanillé\r\n    oeuf\r\n    lait\r\n\r\n    Préchauffez le four à 210°C (thermostat 7). Lavez rapidement les cerises sous un filet d\'eau fraîche, équeutez-les et égouttez-les. Personnellement j\'enlève les noyaux, mais la recette traditionnelle veut qu\'on les laisse, donc c\'est à vous de voir si vous les laissez ou pas... Faites fondre les 40 g de beurre dans une petite casserole à fond épais. Mélangez dans un grand bol la farine, le sucre, le sel et le sucre vanillé. Incorporez les oeufs peu à peu puis le lait petit à petit en continuant de mélanger. Ajoutez le beurre fondu.\r\n    Étape 2\r\n    beurre\r\n    cerises\r\n\r\n    Beurrez grassement le plat, rangez les cerises puis versez la pâte à clafoutis. Mettez au four pendant 10 mn à 210°C puis baissez à 180°C et cuisez encore 20 mn.\r\n    Étape 3\r\n    sucre glace\r\n\r\n    Servez le clafoutis froid ou tiède, saupoudré de sucre glace.', 1, '', 'https://platetrecette.com/wp-content/uploads/2019/07/clafoutis-l%C3%A9ger-aux-cerises-WW-1.jpg', 1, '00:40:00'),
(5, 'Tajine d\'agneau au coing (Tadjin bessferdjel)', 'Dans une cocotte minute ou une marmite faire saisir la viande avec l’huile, ajouter l’oignon haché finement, les gousses d’ail épluchées entières, les épices ensuite ajouter un verre à thé d’eau bouillante laisser imprégner la viande des épices quelques minutes à feu très doux .\r\nDès que la viande prend de la couleur, verser de l’eau bouillante à hauteur de la viande et ajouter le bouquet de coriandre et persil fermer. Laisser cuire à feu moyen pendant environ 20 à 30 minutes jusqu’à ce que la viande soit complètement tendre et la sauce réduise .\r\nEntre temps laver et éplucher les coings, les couper en quartiers égaux puis les frotter avec le citron afin qu’ils ne s’oxydent pas et perdent leur couleur .\r\nDisposer morceaux de coings dans une casserole d’eau à hauteur des coings puis les faire cuire .\r\nPour les confire, garder la casserole sur feu doux et ajouter le beurre, le sucre ou le miel, la pincée de sel et la cannelle en remuant de temps à autre. Au bout de 10 à 15 minutes retirer du feu.\r\nDans un plat de servir disposer les morceaux de viandes, quelques louches de sauce puis mettre les coings tout autours et garnir d’amandes effilées . Servir aussitôt accompagné d’un bon pain maison .\r\nBonne réalisation et bonne dégustation .', 0, 'Pour la viande en sauce :\r\n800 g de viande d’agneau, de veau ou de poulet\r\n1 gros oignon\r\n2 gousses d’ail entières\r\n3 cuillères à soupe d’huile\r\n1/2 cuillère à café de poivre\r\n1/2 cuillère à café de gingembre\r\n1/2 cuillère à café de curcuma\r\nQuelques pistils de safran\r\n1 bâtonnet de cannelle\r\n1 petit bouquet de coriandre et persil\r\n 500 g de coings (2 à 3 coings moyens)\r\nUn demi citron\r\n2 à 3 cuillères à soupe de miel ou de sucre semoule\r\n1 bâtonnet de cannelle\r\n1 cuillère à soupe de beurre\r\n1 pincée de sel\r\n1 poignet d’amandes effilées ou graines de sésame dorées à la poêle', 'http://voyagesenlegumie.be/wp-content/uploads/2018/10/TajineCoings1.jpg', 10, '02:00:00'),
(6, 'Boule de noix de coco à la fleur d\'oranger', '\r\n\r\n    Dans un saladier, battez les œufs avec l’huile puis ajoutez le sucre et la levure. Ajoutez ensuite la farine tamisée et mélangez l’ensemble pour obtenir une pâte homogène.\r\n    Préchauffez le four à 180°C (th6).\r\n    Façonnez des petites boules et déposez-les sur une plaque à four recouverte de papier cuisson.\r\n    Enfournez pour 10 min puis plongez-les dans un bol avec la confiture fondue et l’eau de fleur d’oranger. Roulez-les ensuite dans la noix de coco avant de les déguster !\r\n\r\n', 1, '300g Farine\r\n170g Sucre\r\n170g Huile\r\n2Oeufs\r\n1sachet Levure\r\n150g Confiture(de votre choix)\r\n150g Noix de coco râpée\r\n5cl Eau de fleur d\'oranger\r\nCalories = Moyen', 'https://www.academiedugout.fr/images/17195/948-580/fotolia_51971087_subscription_xxl.jpg?poix=50&poiy=50', 17, '01:00:00'),
(7, 'Namandier aux poires', '\r\n\r\nPelez les poires et coupez-les en lamelles.\r\n\r\nPréchauffez le four à 180° (th 6).\r\n\r\n \r\n2\r\n\r\nFouettez ensemble les œufs, le sucre, le beurre fondu, l’amaretto et l’amande en poudre.\r\n3\r\n\r\nVersez la préparation dans un moule beurré ou siliconé.\r\n4\r\n\r\nRépartissez les poires dans cette préparation.\r\n5\r\n\r\nParsemez d’amandes effilées et de pépites de chocolat.\r\n\r\nEnfournez le 30 minutes.\r\n\r\n \r\n\r\n  \r\n\r\n6\r\n\r\nDémoulez-le quand il est refroidi.\r\n', 1, '    3 grosses poires\r\n    100 g de beurre\r\n    100 g de poudre d\'amandes\r\n    50 g d\' amandes effilées\r\n    2 cs d\' amaretto\r\n    100 g de sucre de canne\r\n    4 oeufs\r\n    60 g de pépites de chocolat', 'https://www.kilometre-0.fr/wp-content/uploads/2019/10/20191019Cuisine_mart254-1.jpg', 5, '01:00:00'),
(8, 'Raisins cuit, au miel et aux amendes (Marqa bel \'assel)', '    Dans un couscoussier mettre les raisins cuire à la vapeur\r\n    Une fois très tendre les enlever et les laisser refroidir\r\n    Mélanger avec du miel ( bien les imbiber) et la pincée de cannelle\r\n    Laisser refroidir durant une nuit pour que le raisin sec absorbe le miel.\r\n    Servir dans des plats,décorer avec des amandes mondées et frites préalablement dans l\'huile\r\n    Servir accompagné d\'une boisson gazeuse.\r\n', 1, '500g de raisins noir\r\n250g de miel\r\nQuelques amandes mondés et frits\r\nUne pincée de cannelle en poudre.\r\n', 'https://recettes.de/images/blogs/safrancannelle/mar-aa-bel-assel.640x480.jpg', 19, '01:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `user_id` int(8) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_surname` varchar(128) NOT NULL,
  `user_name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`user_id`, `user_password`, `user_surname`, `user_name`) VALUES
(25, '$2y$10$/U.hzZ159mT6ZaMH15UukuqfHo8szKQivOtv/qU5F3RkbqdrEdwGm', 'mail@mail.fr', 'sqdqdqsdq');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`);

--
-- Index pour la table `fruit`
--
ALTER TABLE `fruit`
  ADD PRIMARY KEY (`fruit_id`),
  ADD KEY `fruit_color` (`fruit_color`);

--
-- Index pour la table `link_fruit_month`
--
ALTER TABLE `link_fruit_month`
  ADD PRIMARY KEY (`link_fruit`,`link_month`),
  ADD KEY `link_month` (`link_month`);

--
-- Index pour la table `link_fruit_recipe`
--
ALTER TABLE `link_fruit_recipe`
  ADD PRIMARY KEY (`link_fruit`,`link_recipe`);

--
-- Index pour la table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`month_order`);

--
-- Index pour la table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`recipe_id`),
  ADD KEY `recipe_fruit` (`recipe_fruit`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`,`user_surname`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `color`
--
ALTER TABLE `color`
  MODIFY `color_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `fruit`
--
ALTER TABLE `fruit`
  MODIFY `fruit_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `month`
--
ALTER TABLE `month`
  MODIFY `month_order` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `recipe_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `fruit`
--
ALTER TABLE `fruit`
  ADD CONSTRAINT `fruit_ibfk_1` FOREIGN KEY (`fruit_color`) REFERENCES `color` (`color_id`);

--
-- Contraintes pour la table `link_fruit_month`
--
ALTER TABLE `link_fruit_month`
  ADD CONSTRAINT `link_fruit_month_ibfk_1` FOREIGN KEY (`link_fruit`) REFERENCES `fruit` (`fruit_id`),
  ADD CONSTRAINT `link_fruit_month_ibfk_2` FOREIGN KEY (`link_month`) REFERENCES `month` (`month_order`);

--
-- Contraintes pour la table `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `recipe_ibfk_1` FOREIGN KEY (`recipe_fruit`) REFERENCES `fruit` (`fruit_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
